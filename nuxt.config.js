export default {
  publicRuntimeConfig: {
    DISCOURSE: process.env.DISCOURSE,
    DISCOURSE_ES: process.env.DISCOURSE_ES,
    MEMBERS: process.env.MEMBERS,
  },
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Fix issue https://github.com/nuxt-community/tailwindcss-module/issues/480
  devServerHandlers: [],

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Carte Ğ1',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  // plugins: [{ src: '~/plugins/vue-leaflet', mode: 'client' }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontawesome',
  ],

  fontawesome: {
    addCss: true,
    component: 'fa', // component name
    icons: {
      solid: [
        'faHeart',
        'faEye',
        'faCommentAlt',
        'faTags',
        'faSearch',
        'faBackspace',
        'faTimes',
        'faCopy',
        'faUserFriends',
        'faSpinner',
        'faGlobeAfrica',
        'faArrowLeft',
        'faLocationArrow',
        'faShareAlt',
        'faInfo',
        'faExclamationTriangle',
        'faEllipsisV',
        'faComment',
        'faEnvelope',
      ],
      brands: [
        'faDiaspora',
        'faFacebook',
        'faLinkedin',
        'faTwitter',
        'faMastodon',
        'faReddit',
        'faSkype',
        'faTelegram',
        'faWhatsapp',
      ],
    },
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'nuxt-leaflet',
    'nuxt-clipboard',
    [
      'vue-social-sharing/nuxt',
      {
        networks: {
          framasphere: 'https://framasphere.org/bookmarklet?url=@u&title=@t',
        },
      },
    ],
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      name: 'Carte Ğ1',
      favicon: false,
      lang: 'fr',
      theme_color: '#363636',
      ogHost: 'carte.monnaie-libre.fr',
      ogImage: '/screenshot-map-g1.png',
      twitterCard: 'summary_large_image',
      twitterSite: '@monnaie_libre',
      twitterCreator: '@monnaie_libre',
    },
    manifest: {
      name: 'Carte Ğ1',
      short_name: 'Carte Ğ1',
      lang: 'fr',
    },
    icon: {
      fileName: 'icon-app-v1.png',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    purgeCSS: {
      whitelistPatterns: [/-fa$/, /^fa-/],
    }
  }
}
