module.exports = {
  theme: {
    extend: {
      colors: {
        leaflet: '#0078A8',
      },
    },
  },
  variants: {
    extend: {
      padding: ['last'],
    },
  },
  plugins: [],
  purge: {
    content: [
      'components/**/*.vue',
      'mixins/**/*.js',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
      // TypeScript
      'plugins/**/*.ts',
      'nuxt.config.ts',
    ],
  },
}
