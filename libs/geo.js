const geoProvider = 'https://nominatim.openstreetmap.org'
const geoIP = {
  localstorageExpiration: 1000 * 60 * 60 * 24, // 1 day
  providers: [
    { url: 'https://ipapi.co/json/' },
    { url: 'http://ip-api.com/json', coords: ['lat', 'lon'] },
    {
      url: `https://api.ipgeolocation.io/ipgeo?apiKey=${process.env.IPGEOLOCATION}`,
    },
    {
      url: `https://api.ipfind.com/me?auth=${process.env.IPFIND}`,
    },
    {
      url: `https://api.ipdata.co/?api-key=${process.env.IPDATA}`,
    },
  ],
}

export default (app) => {
  return {
    /**
     * Default map center
     * @memberof $px.geo
     * @constant
     */
    mapDefaultCenter: [46.227638, 2.213749],

    /**
     * Default zoom level
     * @memberof $px.geo
     * @constant
     */
    mapDefaultZoom: 6,

    /**
     * Return SQL Location object
     *
     * @memberof $px.geo
     * @param {String} lon
     * @param {String} lat
     * @return {Object} Location object
     */
    getLocation(lon, lat) {
      return { type: 'Point', coordinates: [parseFloat(lon), parseFloat(lat)] }
    },

    /**
     * Return leflet/openstreetmap coordinate object from location object
     *
     * @memberof $px.geo
     * @param {Object} location - Location object
     * @return {Object} Coordinate object
     */
    getCoordinates(location) {
      return {
        lon: location.coordinates[0],
        lat: location.coordinates[1],
      }
    },

    /**
     * Return city from openstreetmap address
     *
     * @memberof $px.geo
     * @param {Object} address
     * @return {String} Country
     */
    getCity(address) {
      return (
        address.village ||
        address.town ||
        address.city ||
        address.river ||
        address.county ||
        address.neighbourhood ||
        address.address29 ||
        address.path ||
        address.ruins ||
        address.locality ||
        address.natural
      )
    },

    /**
     *  Fetch geoProvider to get location from LatLng
     *
     * @memberof $px.geo
     * @param {Object} LatLng
     * @returns {Promise} openstreetmap location
     */
    geoReverse(latLng) {
      const url = new URL(`${geoProvider}/reverse`)
      url.search = new URLSearchParams({
        format: 'json',
        addressdetails: 1,
        lat: latLng.lat,
        lon: latLng.lng,
      }).toString()

      return fetch(url, {
        headers: {
          'accept-language': 'fr-FR',
        },
      })
        .then((response) => response.json())
        .then((json) => json)
        .catch((err) => console.log(err))
    },

    /**
     *  Fetch geoProvider to get LatLng from location
     *
     * @memberof $px.geo
     * @param {String} location - The place to search
     * @returns {Promise} LatLng geolocation
     */
    geoSearch(location) {
      // if location length < 2 return fake promise, don't query geoProvider.
      if (location.trim().length < 2)
        return {
          then: (fn) => fn([]),
        }

      const url = new URL(`${geoProvider}/search`)
      url.search = new URLSearchParams({
        format: 'json',
        addressdetails: 1,
        q: location.trim(),
      }).toString()

      return fetch(url, {
        // Set language header for nominatim to return correct language in response
        headers: {
          'accept-language': 'fr-FR',
        },
      })
        .then((response) => response.json())
        .then((response) => {
          response.forEach((item) => {
            item.bounds = [
              [item.boundingbox[0], item.boundingbox[2]],
              [item.boundingbox[1], item.boundingbox[3]],
            ]
            return item
          })
          return response
        })
        .catch((err) => console.log(err))
    },

    /**
     *  Fetch geoProvider to get location from <osm_type+osm_id>
     *
     * @memberof $px.geo
     * @param {String} osmId - A string with the first capital letter of osm_type and the osm_id
     * @returns {Promise} openstreetmap location
     */
    geoLookup(osmId) {
      return app.$axios.$get(`${geoProvider}/lookup?`, {
        params: {
          format: 'json',
          osm_ids: osmId,
        },
        headers: {
          'accept-language': 'fr-FR',
        },
      })
    },

    /**
     * Return client Browser geolocation capabilities.
     *
     * @return {Boolean} Browser can geolocate or not.
     */
    checkSupportGeolocation() {
      return (
        typeof window !== 'undefined' && navigator && 'geolocation' in navigator
      )
    },

    /**
     * Return promise with coordinates of client Browser.
     * That function prompt user throw native navigator popup for permission to geolocate.
     *
     * @param {Object} options - native getCurrentPosition options.
     * @return {Promise} A promise that return coordinates.
     */
    getCurrentPosition(options = {}) {
      return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
          async ({ coords }) => {
            const { latitude, longitude, altitude, accuracy } = coords
            let geoLoc = (await this.getClientGeoIP()) || {}
            geoLoc = {
              fromNavigator: true,
              ...geoLoc,
              ...{ latitude, longitude, altitude, accuracy },
            }

            const geo = await this.geoReverse({
              lat: latitude,
              lng: longitude,
            })
            if (geo && geo.address)
              geoLoc = Object.assign(geoLoc, geo.address, geo)

            this.setClientGeoIP(geoLoc)
            resolve(geoLoc)
          },
          (err) => {
            app.$px.trackError(err)
            app.$px.buefy.error('no-browser-location')
          },
          options
        )
      })
    },

    /**
     * Fetch with timeout.
     * @memberof $px
     * @param {String} url - Url to fetch
     * @param {?String} [ms=2000] - Asynchronous function to timeout
     */
    fetchTimeout(url, timeout = 2000) {
      return new Promise((resolve, reject) => {
        // Set timeout timer
        const timer = setTimeout(
          () => reject(new Error('Request timed out')),
          timeout
        )

        fetch(url)
          .then(
            (response) => resolve(response),
            (err) => reject(err)
          )
          .finally(() => clearTimeout(timer))
      })
    },

    /**
     * Return promise with coordinates of client Browser from provider.
     * That function prompt user throw native navigator popup for permission to geolocate.
     *
     * @param {Number} providerIndex - Index of geo-provider in const.
     * @return {Promise} A promise that return coordinates.
     */
    async getClientGeoIP(providerIndex = 0) {
      // check if there is already geo
      const clientGeo = localStorage.client_geo
      if (clientGeo) return JSON.parse(clientGeo)

      // Stop looping provider
      if (providerIndex === geoIP.providers.length) return false

      try {
        const response = await this.fetchTimeout(
          geoIP.providers[providerIndex].url
        )
          .then((response) => response.json())
          .catch((error) => {
            return false && error // Stop here, don't fetch another provider and stop propagate error!
          })
        if (response && (response.latitude || response.lat)) {
          const coords = geoIP.providers[providerIndex].coords
          if (coords) {
            response.latitude = response[coords[0]]
            response.longitude = response[coords[1]]
          }

          response.timestamp = Date.now()
          this.setClientGeoIP(response)
          return response
        }

        return this.getClientGeoIP(providerIndex + 1)
      } catch (err) {
        console.log(err)
        return false
      }
    },

    /**
     * Return promise with coordinates of client Browser from provider.
     * That function prompt user throw native navigator popup for permission to geolocate.
     *
     * @param {Object} geoData - Index of geo-provider in const.
     * @return null Set cookie.
     */
    setClientGeoIP(geoData) {
      localStorage.setItem('client_geo', JSON.stringify(geoData))
    },
  }
}
