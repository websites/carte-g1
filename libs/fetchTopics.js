async function fetchTopics(url) {
  return await fetch(url)
    .then((resp) => resp.json())
    .then((data) => {
      const topics = data.topic_list.topics
        // parse topics with geolocation and remove old topic
        .filter((topic) => {
          let outdated = true // allow all topics
          if (topic.event_ends_at) {
            const eventEnd = new Date(
              topic.event_starts_at || topic.event_ends_at
            ).getTime()
            outdated = Date.now() > eventEnd
          }

          return topic.location && topic.location.geo_location && !outdated
        })
        // and feed each one with creator object
        .map((topic) => {
          return {
            ...topic,
            ...{
              creator: data.users.filter(
                (user) => user.id === topic.posters[0].user_id
              )[0],
            },
          }
        })

      return topics
    })
}

export default fetchTopics
