# Carte Ğ1

## Utilisation

**Pour ouvrir et zoomer sur une popup d'un groupe local ou d'un évènement, indiquez l'`id` du sujet du forum en paramètres url.**

Exemple :

Lien du forum : [https://forum.monnaie-libre.fr/t/10-aout-2019-pique-nique-g1-a-rennes/**7273**](https://forum.monnaie-libre.fr/t/10-aout-2019-pique-nique-g1-a-rennes/7273)

    https://carte.monnaie-libre.fr/?id=7273

**Pour ouvrir et zoomer sur une popup d'un compte, indiquez la `pubkey` du compte en paramètres url.**

Exemple :

    https://carte.monnaie-libre.fr/?pubkey=2JggyyUn2puL5PG6jsMYFC2y9KwjjMmy2adnx3c5fUf8

**Pour afficher une partie de la carte, indiquez le `zoom` et les coordonnées `lat` et `lon` en paramètres d'url.**

Exemple :

    https://carte.monnaie-libre.fr/?zoom=10&lat=43.62&lon=1.49

**Intégration IFRAME :**

La carte 100% width et height donc vous pouvez mettre les dimensions que vous voulez pour votre iframe.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Test iframe</title>
  </head>
  <body>
    <iframe
      src="https://carte.monnaie-libre.fr/"
      frameborder="0"
      style="width:100%; height:100vh"
    ></iframe>
  </body>
</html>
```

Vous pouvez masquer l'interface utilisateur, les couches des membres ou des topics. Regardez le bouton "partage" pour voir comment cela fonctionne.

# Build Setup

`git clone` this repository.
Set discourse and members endpoint in `.env` file. See `.env.example`
For PWA and favicon, change `icon-app-v1.png` and `favicon.png` in `/static` directory.

## Install dependencies

    pnpm i

# For development, serve with hot reload at localhost:3000

    pnpm dev

You can use files in `/static` directory for fake data. Set `DISCOURSE=http://localhost:3000` will use `map.json` file and `MEMBERS=http://localhost:3000/geoloc-members.json` to use fake members.

# Build for production

    pnpm generate

All files are in `/dist`.

# Serve `/dist` server on localhost

    pnpm start

# Generate static project

    pnpm generate

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
