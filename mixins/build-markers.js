import Vue from 'vue'
import 'leaflet.markercluster'
import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'
import 'leaflet.featuregroup.subgroup'

import fetchTopics from '~/libs/fetchTopics'
import PopupTopic from '~/components/PopupTopic.vue'
import PopupMember from '~/components/PopupMember.vue'

export default {
  data() {
    return {
      control: null,
      clusterTopics: null,
      clusterMembers: null,
    }
  },
  mounted() {
    const collapsed = window.innerWidth < 1024
    this.control = this.$L.control.layers(null, null, { collapsed })
    this.$nextTick(() => {
      if (this.ui) this.control.addTo(this.map)
    })
  },
  methods: {
    async buildTopics() {
      console.log(this.$config)
      const data = await fetchTopics(
        `${this.$config.DISCOURSE}/map.json?order=default`
        // `${this.$config.DISCOURSE}/discourse-post-event/events.json?include_details=true`
      )
      const dataES = await fetchTopics(
        `${this.$config.DISCOURSE_ES}/map.json?order=default`
      )

      // Create topics markerCluster
      this.clusterTopics = this.$L.markerClusterGroup({
        chunkedLoading: true,
      })

      // Build topics icons for event and local groups
      const buildIcon = (color) =>
        this.$L.icon(
          Object.assign(this.$L.Icon.Default.prototype.options, {
            iconUrl: `marker-${color}.png`,
            iconRetinaUrl: `marker-${color}.png`,
            iconSize: [29, 40],
            iconAnchor: [14, 40],
            shadowAnchor: [12, 42],
          })
        )
      const blueIcon = buildIcon('blue')
      const redIcon = buildIcon('red')

      // Add data for Lokijs and build events and local groups layers
      const markersEvents = []
      const markersLocals = []

      const filteredDataFR = data.map((topic) => {
        // Create marker
        const marker = this.$L.marker(
          [topic.location.geo_location.lat, topic.location.geo_location.lon],
          {
            title: topic.title,
            icon: topic.event_starts_at ? redIcon : blueIcon,
          }
        )

        // Dynamically create, bind popup to marker and open popup for performance
        marker.on('click', () => {
          this.$store.commit('onOpenPopup', { id: topic.id })
          if (!marker._popup) {
            const popup = this.compilePopup({ topic }, PopupTopic)
            marker.bindPopup(popup, { closeOnClick: false }).openPopup()
          }
        })

        // Add marker to corresponding layer
        topic.event_starts_at ? markersEvents.push(marker) : markersLocals.push(marker)

        // Add marker data to lokijs
        return { ...topic, marker, country: 'fr' }
      })

      const filteredDataES = dataES.map((topic) => {
        // Create marker
        const marker = this.$L.marker(
          [topic.location.geo_location.lat, topic.location.geo_location.lon],
          {
            title: topic.title,
            icon: topic.event_starts_at ? redIcon : blueIcon,
          }
        )

        // Dynamically create, bind popup to marker and open popup for performance
        marker.on('click', () => {
          this.$store.commit('onOpenPopup', { id: topic.id })
          if (!marker._popup) {
            const popup = this.compilePopup({ topic, country: 'es' }, PopupTopic)
            marker.bindPopup(popup, { closeOnClick: false }).openPopup()
          }
        })

        // Add marker to corresponding layer
        topic.event_starts_at ? markersEvents.push(marker) : markersLocals.push(marker)

        // Add marker data to lokijs
        return { ...topic, marker }
      })

      this.topics.insert([...filteredDataFR, ...filteredDataES])

      // Bind subgroups to clusterTopics layer
      const groupEvents = this.$L.featureGroup.subGroup(
        this.clusterTopics,
        markersEvents
      )
      // const groupLocals = this.$L.featureGroup.subGroup(
      //   this.clusterTopics,
      //   markersLocals
      // )

      // Add layers to control
      if (this.ui) {
        this.control.addOverlay(
          groupEvents,
          '<img src="marker-red-small.png" class="inline mx-1 transform -translate-y-0.5" />Évènements'
        )
        // this.control.addOverlay(
        //   groupLocals,
        //   '<img src="marker-blue-small.png" class="inline mx-1 transform -translate-y-0.5" />Groupes locaux'
        // )
      }

      // Add to the map. Use nextTick because map isn't instanciated at mount time.
      this.$nextTick(() => {
        this.clusterTopics.addTo(this.map)
        groupEvents.addTo(this.map)
        // groupLocals.addTo(this.map)

        // Open popup if topic id is provided
        const topicId = Number(this.$route.query.id)
        if (topicId) {
          const topic = this.topics.find({ id: { $eq: topicId } })
          // Topic may be deleted
          if (topic.length) this.openPopup(topic[0])
        }
      })
    },
    async buildMembers() {
      const response = await fetch(this.$config.MEMBERS)
      const data = await response.json()

      // Store members fetch time
      this.$store.commit('setFetchMembersTime', data.time)

      // Create pane
      this.map.createPane('members-pane').style.zIndex = 410

      // Create topics markerCluster
      this.clusterMembers = this.$L.markerClusterGroup({
        clusterPane: 'members-pane',
        // chunkedLoading: true,
        maxClusterRadius: 60,
        polygonOptions: {
          color: '#aaa',
        },
        iconCreateFunction: (cluster) => {
          const childCount = cluster.getChildCount()
          const size =
            childCount < 10
              ? 'h-4 w-4'
              : childCount < 100
                ? 'h-6 w-6'
                : 'h-8 w-8'
          return this.$L.divIcon({
            html: `<div class="${size} member-cluster">${childCount}</div>`,
            className: '',
          })
        },
      })

      // Build members icons for certified members and wallets
      const buildIcon = (color) =>
        this.$L.divIcon({
          className: `member-dot ${color}`,
          iconSize: [10, 10],
        })
      const blueIcon = buildIcon('bg-blue-600')
      const grayIcon = buildIcon('bg-gray-500')

      // Add data for Lokijs and build members
      const certifieds = []
      const wallets = []
      this.members.insert(
        data.wallets.map((member) => {
          // Create marker
          const marker = this.$L.marker(
            [member.geoPoint.lat, member.geoPoint.lon],
            {
              title: member.title,
              icon: member.isMember ? blueIcon : grayIcon,
            }
          )

          // Dynamically create, bind popup to marker and open popup for performance
          marker.on('click', (e) => {
            this.$store.commit('onOpenPopup', { pubkey: member.pubkey })
            if (!marker._popup) {
              const popup = this.compilePopup({ member }, PopupMember)
              marker.bindPopup(popup, { closeOnClick: false }).openPopup()
              console.log(e, marker)

              // const px = this.map.project(e.target._popup._latlng) // find the pixel location on the map where the popup anchor is
              // px.y -= e.target._popup._container.clientHeight / 2 // find the height of the popup container, divide by 2, subtract from the Y axis of marker location
              // this.map.panTo(this.map.unproject(px), { animate: true }) // pan to new center
            }
          })

          // Add marker to corresponding layer
          member.isMember ? certifieds.push(marker) : wallets.push(marker)

          // Add marker data to lokijs
          return { ...member, marker }
        })
      )

      // Bind subgroups to clusterMembers layer
      const groupCertifieds = this.$L.featureGroup.subGroup(
        this.clusterMembers,
        certifieds
      )
      const groupWallets = this.$L.featureGroup.subGroup(
        this.clusterMembers,
        wallets
      )

      // Add layers to control
      if (this.ui) {
        this.control.addOverlay(
          groupCertifieds,
          '<div style="width:10px;height:10px" class="bg-blue-600 inline-block ml-1 mr-1.5 opacity-75 rounded-full"></div>Membres certifiés'
        )
        this.control.addOverlay(
          groupWallets,
          '<div style="width:10px;height:10px" class="bg-gray-500 inline-block ml-1 mr-1.5 opacity-75 rounded-full"></div>Comptes portefeuille'
        )
      }

      // Add to the map
      this.$nextTick(() => {
        this.clusterMembers.addTo(this.map)
        groupCertifieds.addTo(this.map)
        groupWallets.addTo(this.map)

        // Open popup if pubkey id is provided
        const pubkey = this.$route.query.pubkey
        if (pubkey) {
          const member = this.members.find({ pubkey: { $eq: pubkey } })
          // member may be deleted
          if (member.length) this.openPopup(member[0])
        }
      })
    },
    compilePopup(propsData, popupComponent) {
      // Compile html from vue component and add it to popup
      return this.$L
        .popup({
          maxWidth: Math.min(500, window.innerWidth),
          minWidth: 300,
          autoPanPadding: window.innerWidth > 620 ? [40, 60] : [0, 0],
        })
        .setContent(
          new Vue(
            Object.assign(
              { propsData },
              { $config: this.$config },
              popupComponent
            )
          ).$mount().$el
        )
    },
  },
}
