export const state = () => ({
  geoControlClosed: false,
  openedPopup: null,
  fetchMembersTime: null,
})

export const getters = {
  hasPopup: (state) => {
    return state.openedPopup !== null
  },
}

export const mutations = {
  onCloseGeoControl(state) {
    state.geoControlClosed = true
  },
  onOpenPopup(state, popup) {
    state.openedPopup = popup
    const params = '?' + new URLSearchParams(popup).toString()
    history.replaceState({}, null, document.location.pathname + params)
  },
  onClosePopup(state) {
    state.openedPopup = null
  },
  setFetchMembersTime(state, time) {
    state.fetchMembersTime = time
  },
}
